<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20191113093902 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE apartment');
        $this->addSql('ALTER TABLE advert ADD zip_code VARCHAR(16) NOT NULL, ADD city VARCHAR(32) NOT NULL, ADD street VARCHAR(64) NOT NULL, ADD building VARCHAR(8) NOT NULL, ADD apartment_number VARCHAR(8) DEFAULT NULL, DROP apartment_id');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE apartment (id INT AUTO_INCREMENT NOT NULL, zip_code VARCHAR(16) NOT NULL COLLATE utf8_unicode_ci, city VARCHAR(32) NOT NULL COLLATE utf8_unicode_ci, street VARCHAR(64) NOT NULL COLLATE utf8_unicode_ci, building VARCHAR(8) NOT NULL COLLATE utf8_unicode_ci, apartment_number VARCHAR(8) DEFAULT NULL COLLATE utf8_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE advert ADD apartment_id INT NOT NULL, DROP zip_code, DROP city, DROP street, DROP building, DROP apartment_number');
    }
}
