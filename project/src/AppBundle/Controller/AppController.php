<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 11/13/2019
 * Time: 11:39 AM
 */

namespace AppBundle\Controller;

use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AppController extends Controller
{
    public $entityClassName = null;

    /**
     * @param $id
     * @return mixed
     * @throws Exception
     */
    protected function findEntity($id)
    {
        if ($this->entityClassName === null) {
            throw new Exception();
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entity = $entityManager->getRepository($this->entityClassName)->findOneById($id);

        if ($entity == null) {
            throw $this->createNotFoundException();
        }

        return $entity;
    }
}
