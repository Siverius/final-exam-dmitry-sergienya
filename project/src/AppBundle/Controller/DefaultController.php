<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Advert;
use AppBundle\Form\AdvertType;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DefaultController
 * @package AppBundle\Controller
 */
class DefaultController extends AppController
{
    /**
     * DefaultController constructor.
     */
    public function __construct()
    {
        $this->entityClassName = Advert::class;
    }

    /**
     * @Route("/new", name="new", methods={"POST", "GET"})
     * @Route("/{id}/edit", name="edit", requirements={"id"="\d+"}, methods={"POST", "GET"})
     *
     * @param Request $request
     * @param null $id
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function editAction(Request $request, $id = null)
    {
        $advert = (null !== $id) ? $this->findEntity($id) : new Advert();
        $new = $advert->isNewRecord;

        $form = $this->createForm(AdvertType::class, $advert);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $advert = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($advert);
            $entityManager->flush();

            $message = $new ? "flash.create.success" : "flash.edit.success";
            $this->addFlash('success', $message);
        }

        return $this->render('default/edit.html.twig', [
            'new' => $new,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{pageId}", name="index", requirements={"pageId"="\d+"}, methods={"GET"}, defaults={"pageId"=null})
     *
     * @param PaginatorInterface $paginator
     * @param int $pageId
     * @return Response
     */
    public function indexAction(PaginatorInterface $paginator, $pageId = 1)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $adverts = $entityManager->getRepository(Advert::class)->findAll();

        $pagination = $paginator->paginate(
            $adverts,
            $pageId,
            10
        );

        return $this->render('default/index.html.twig',
            [
                'pagination' => $pagination,
            ]
        );

    }

    /**
     * @Route("/{id}", name="show", requirements={"id"="\d+"}, methods={"GET"})
     *
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function showAction($id)
    {
        $advert = $this->findEntity($id);

        return $this->render('default/show.html.twig', ['advert' => $advert]);
    }

    /**
     * @Route("/{id}/delete", name="delete", requirements={"id"="\d+"}, methods={"DELETE"})
     *
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function deleteAction($id)
    {
        $entity = $this->findEntity($id);

        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();

        $this->addFlash(
            'info',
            'flash.delete.success'
        );

        return $this->redirectToRoute('index');
    }

}
