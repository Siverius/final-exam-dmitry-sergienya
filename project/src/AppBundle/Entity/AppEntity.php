<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 11/13/2019
 * Time: 11:51 AM
 */

namespace AppBundle\Entity;


class AppEntity
{
    public $isNewRecord;

    public function __construct()
    {
        $this->isNewRecord = (null == $this->getId());
    }
}