<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

class AdvertType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'title',
                TextType::class,
                [
                    'label' => 'label.title',
                    'constraints' => [
                        new NotBlank(),
                    ]
                ]
            )
            ->add(
                'contactPhone',
                TelType::class,
                [
                    'label' => 'label.contactPhone',
                    'constraints' => [
                        new NotBlank(),
                        new Regex([
                            'pattern' => "/(([+][(]?[0-9]{1,3}[)]?)|([(]?[0-9]{4}[)]?))\s*[)]?[-\s\.]?[(]?[0-9]{1,3}[)]?([-\s\.]?[0-9]{3})([-\s\.]?[0-9]{3,4})/",
                            'message' => "message.wrongTelNumber",
                        ]),
                    ]
                ]
            )
            ->add(
                'zipCode',
                TextType::class,
                [
                    'label' => 'label.zipCode',
                    'constraints' => [
                        new NotBlank(),
                    ]
                ]
            )
            ->add(
                'city',
                TextType::class,
                [
                    'label' => 'label.city',
                    'constraints' => [
                        new NotBlank(),
                    ]
                ]
            )
            ->add(
                'street',
                TextType::class,
                [
                    'label' => 'label.street',
                    'constraints' => [
                        new NotBlank(),
                    ]
                ]
            )
            ->add(
                'building',
                TextType::class,
                [
                    'label' => 'label.building',
                    'constraints' => [
                        new NotBlank(),
                    ]
                ]
            )
            ->add(
                'apartmentNumber',
                TextType::class,
                [
                    'label' => 'label.apartmentNumber',
                    'constraints' => [
                        new NotBlank(),
                    ]
                ]
            )
            ->add('save', SubmitType::class);

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Advert'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_advert';
    }


}
